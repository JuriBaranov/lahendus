window.onload = function () {
    document.querySelector('.päev').value = (new Date()).toISOString().substr(0, 10);
    document.querySelector('.alg').value = (new Date()).toISOString().substr(0, 10);
    var k = new Date(document.getElementById("alg").value);
    document.querySelector('.l6pp').value = (new Date(k.setDate(k.getDate() + 14)).toISOString().substr(0, 10));
};

function graaf() {
    var a = document.getElementById("alg").value;
    var l = document.getElementById("l6pp").value;
    var la = Number(document.getElementById("asukoht").value);
    var lo = Number(document.getElementById("asukoht2").value);
    var d = [];
    var kp = [];
    for (i = new Date(a); i <= new Date(l); i.setDate(i.getDate() + 1)) {
        var date = new Date(i);
        var times = SunCalc.getTimes((date), la, lo);

        var pikkus = times.sunset - times.sunrise;
        var pikkusStr = Math.floor((pikkus / 1000 / 60 / 60) % 24) + (Math.floor((pikkus / 1000 / 60) % 60) * 0.01);
        d.push(pikkusStr);
        kp.push(date.getMonth() + "-" + date.getDate());
    }
    new Chart(document.getElementById("line"), {
        type: 'line',
        data: {
            labels: kp,
            datasets: [{
                data: d,
                label: "Asukoht: " + la + "," + lo + "",
                borderColor: "#E6E6FA",
                fill: false
            }]
        },

        options: {
            title: {
                display: true,
                 text: 'Päeva pikkuse muutumine perioodil: ' + a + " - " + l + ""
            }

        }
    });
}